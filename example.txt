Name:               router-1-gg8z7
Namespace:          default
Priority:           0
PriorityClassName:  <none>
Node:               fedora3.mguginolocal.com/192.168.124.50
Start Time:         Tue, 24 Jul 2018 17:54:07 +0000
Labels:             deployment=router-1
                    deploymentconfig=router
                    router=router
Annotations:        openshift.io/deployment-config.latest-version=1
                    openshift.io/deployment-config.name=router
                    openshift.io/deployment.name=router-1
                    openshift.io/scc=hostnetwork
Status:             Running
IP:                 192.168.124.50
Controlled By:      ReplicationController/router-1
Containers:
  router:
    Container ID:   docker://86a61e6bd2befa648bacba3a0190ae67fc8a2ab0c191da3fc26d7d8497355d78
    Image:          docker.io/openshift/origin-haproxy-router:v3.11.0
    Image ID:       docker-pullable://docker.io/openshift/origin-haproxy-router@sha256:1ccd4ffe762589208953311cef0ec85df4a8ef58522009cb5c3f6241eaa24b86
    Ports:          80/TCP, 443/TCP, 1936/TCP
    Host Ports:     80/TCP, 443/TCP, 1936/TCP
    State:          Running
      Started:      Tue, 24 Jul 2018 17:54:20 +0000
    Ready:          True
    Restart Count:  0
    Requests:
      cpu:      100m
      memory:   256Mi
    Liveness:   http-get http://localhost:1936/healthz delay=10s timeout=1s period=10s #success=1 #failure=3
    Readiness:  http-get http://localhost:1936/healthz/ready delay=10s timeout=1s period=10s #success=1 #failure=3
    Environment:
      DEFAULT_CERTIFICATE_DIR:                /etc/pki/tls/private
      DEFAULT_CERTIFICATE_PATH:               /etc/pki/tls/private/tls.crt
      ROUTER_CIPHERS:
      ROUTER_EXTERNAL_HOST_HOSTNAME:
      ROUTER_EXTERNAL_HOST_HTTPS_VSERVER:
      ROUTER_EXTERNAL_HOST_HTTP_VSERVER:
      ROUTER_EXTERNAL_HOST_INSECURE:          false
      ROUTER_EXTERNAL_HOST_INTERNAL_ADDRESS:
      ROUTER_EXTERNAL_HOST_PARTITION_PATH:
      ROUTER_EXTERNAL_HOST_PASSWORD:
      ROUTER_EXTERNAL_HOST_PRIVKEY:           /etc/secret-volume/router.pem
      ROUTER_EXTERNAL_HOST_USERNAME:
      ROUTER_EXTERNAL_HOST_VXLAN_GW_CIDR:
      ROUTER_LISTEN_ADDR:                     0.0.0.0:1936
      ROUTER_METRICS_TLS_CERT_FILE:           /etc/pki/tls/metrics/tls.crt
      ROUTER_METRICS_TLS_KEY_FILE:            /etc/pki/tls/metrics/tls.key
      ROUTER_METRICS_TYPE:                    haproxy
      ROUTER_SERVICE_HTTPS_PORT:              443
      ROUTER_SERVICE_HTTP_PORT:               80
      ROUTER_SERVICE_NAME:                    router
      ROUTER_SERVICE_NAMESPACE:               default
      ROUTER_SUBDOMAIN:
      ROUTER_THREADS:                         0
      STATS_PASSWORD:                         qBTNBPD34D
      STATS_PORT:                             1936
      STATS_USERNAME:                         admin
      EXTENDED_VALIDATION:                    true
    Mounts:
      /etc/pki/tls/metrics/ from metrics-server-certificate (ro)
      /etc/pki/tls/private from server-certificate (ro)
      /var/run/secrets/kubernetes.io/serviceaccount from router-token-wn94f (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  metrics-server-certificate:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  router-metrics-tls
    Optional:    false
  server-certificate:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  router-certs
    Optional:    false
  router-token-wn94f:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  router-token-wn94f
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  node-role.kubernetes.io/infra=true
Tolerations:     node.kubernetes.io/memory-pressure:NoSchedule
Events:          <none>


Name:               router-1-pt84q
Namespace:          default
Priority:           0
PriorityClassName:  <none>
Node:               fedora2.mguginolocal.com/192.168.124.233
Start Time:         Thu, 26 Jul 2018 16:43:51 +0000
Labels:             deployment=router-1
                    deploymentconfig=router
                    router=router
Annotations:        openshift.io/deployment-config.latest-version=1
                    openshift.io/deployment-config.name=router
                    openshift.io/deployment.name=router-1
                    openshift.io/scc=hostnetwork
Status:             Running
IP:                 192.168.124.233
Controlled By:      ReplicationController/router-1
Containers:
  router:
    Container ID:   docker://bd7865a006bac748e0169c1111a09f15478b636bde42762cf47209a4a560023c
    Image:          docker.io/openshift/origin-haproxy-router:v3.11.0
    Image ID:       docker-pullable://docker.io/openshift/origin-haproxy-router@sha256:1ccd4ffe762589208953311cef0ec85df4a8ef58522009cb5c3f6241eaa24b86
    Ports:          80/TCP, 443/TCP, 1936/TCP
    Host Ports:     80/TCP, 443/TCP, 1936/TCP
    State:          Running
      Started:      Thu, 02 Aug 2018 05:22:30 +0000
    Last State:     Terminated
      Reason:       Error
      Exit Code:    255
      Started:      Wed, 01 Aug 2018 21:12:09 +0000
      Finished:     Thu, 02 Aug 2018 05:22:24 +0000
    Ready:          True
    Restart Count:  4
    Requests:
      cpu:      100m
      memory:   256Mi
    Liveness:   http-get http://localhost:1936/healthz delay=10s timeout=1s period=10s #success=1 #failure=3
    Readiness:  http-get http://localhost:1936/healthz/ready delay=10s timeout=1s period=10s #success=1 #failure=3
    Environment:
      DEFAULT_CERTIFICATE_DIR:                /etc/pki/tls/private
      DEFAULT_CERTIFICATE_PATH:               /etc/pki/tls/private/tls.crt
      ROUTER_CIPHERS:
      ROUTER_EXTERNAL_HOST_HOSTNAME:
      ROUTER_EXTERNAL_HOST_HTTPS_VSERVER:
      ROUTER_EXTERNAL_HOST_HTTP_VSERVER:
      ROUTER_EXTERNAL_HOST_INSECURE:          false
      ROUTER_EXTERNAL_HOST_INTERNAL_ADDRESS:
      ROUTER_EXTERNAL_HOST_PARTITION_PATH:
      ROUTER_EXTERNAL_HOST_PASSWORD:
      ROUTER_EXTERNAL_HOST_PRIVKEY:           /etc/secret-volume/router.pem
      ROUTER_EXTERNAL_HOST_USERNAME:
      ROUTER_EXTERNAL_HOST_VXLAN_GW_CIDR:
      ROUTER_LISTEN_ADDR:                     0.0.0.0:1936
      ROUTER_METRICS_TLS_CERT_FILE:           /etc/pki/tls/metrics/tls.crt
      ROUTER_METRICS_TLS_KEY_FILE:            /etc/pki/tls/metrics/tls.key
      ROUTER_METRICS_TYPE:                    haproxy
      ROUTER_SERVICE_HTTPS_PORT:              443
      ROUTER_SERVICE_HTTP_PORT:               80
      ROUTER_SERVICE_NAME:                    router
      ROUTER_SERVICE_NAMESPACE:               default
      ROUTER_SUBDOMAIN:
      ROUTER_THREADS:                         0
      STATS_PASSWORD:                         qBTNBPD34D
      STATS_PORT:                             1936
      STATS_USERNAME:                         admin
      EXTENDED_VALIDATION:                    true
    Mounts:
      /etc/pki/tls/metrics/ from metrics-server-certificate (ro)
      /etc/pki/tls/private from server-certificate (ro)
      /var/run/secrets/kubernetes.io/serviceaccount from router-token-wn94f (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  metrics-server-certificate:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  router-metrics-tls
    Optional:    false
  server-certificate:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  router-certs
    Optional:    false
  router-token-wn94f:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  router-token-wn94f
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  node-role.kubernetes.io/infra=true
Tolerations:     node.kubernetes.io/memory-pressure:NoSchedule
Events:          <none>
