#!/usr/bin/env python
'''Parse terrible output from kubectl describe to something sensible'''
import re
import pprint
import json

# top-level objects start with a capital letter at the start of a line.
NEW_DICT_PATTERN = "^[A-Z]"
SUB_DICT_PATTERN = "^\s+.*:(\s|$)"
new_dict_regex = re.compile(NEW_DICT_PATTERN)
sub_dict_regex = re.compile(SUB_DICT_PATTERN)

def get_data():
    '''reads data from stdin'''
    with open('e3.txt') as f:
        return f.read()


def line_type(line):
    if new_dict_regex.match(line):
        return "tld"
    elif sub_dict_regex.match(line):
        return "subdict"
    else:
        return "string"


def parse_line(line):
    line_parts = line.split(':')
    new_key = line_parts[0]
    # there might be more than two elements, such as a url or timestamp
    unsplit_value = ':'.join(line_parts[1:]).strip()
    if unsplit_value == '\n':
        unsplit_value == ''
    return (new_key, unsplit_value)


def parse_data(data):
    '''parse read data into something usable'''
    items = data.split('\n\n\n')
    items_out = []

    while items:
        item = items.pop().strip('\n\n')
        parsed_dict = {}
        item_lines = item.split('\n')
        previous_indent = []
        parent_dict_stack = []
        while item_lines:
            line = item_lines.pop(0)
            current_type = line_type(line)
            if item_lines:
                nl_type = line_type(item_lines[0])
            else:
                nl_type = "eof"
            if current_type == 'string':
                if line == '':
                    continue
                current_list.append(line.strip())
            elif current_type == 'subdict':
                print('subdict: {}'.format(line))
                new_key, unsplit_value = parse_line(line)
                new_key_stripped = new_key.strip()
                padding = len(new_key) - len(new_key_stripped)
                if previous_indent:
                    print('previous indent found')
                    if previous_indent[-1] < padding:
                        parent_dict_stack.append(parent_dict)
                        previous_indent.append(padding)
                    elif previous_indent[-1] == padding:
                        # We have a peer element to the previous dictionary.
                        pass
                    else:
                        # previous indent was greater, we're walking back.
                        while True:
                            previous_indent.pop()
                            parent_dict = parent_dict_stack.pop()
                            if not previous_indent or previous_indent[-1] < padding:
                                previous_indent.append(padding)
                                parent_dict_stack.append(parent_dict)
                                break
                else:
                    print("appending parent stack")
                    parent_dict_stack.append(parent_dict)
                    previous_indent.append(padding)

                if nl_type == 'subdict':
                    print('next line sd')
                    next_key, next_value = parse_line(item_lines[0])
                    next_key_stripped = next_key.strip()
                    next_pad = len(next_key) - len(next_key_stripped)
                    if next_pad > padding:
                        print('next pad greater')
                        # We know the next item is a child dict
                        parent_dict[new_key_stripped] = {}
                        print(unsplit_value)
                        if unsplit_value != '':
                            print('unsplit none')
                            # some dictionaries unfortunately supply
                            # a value on the same line as dict definition.
                            parent_dict[new_key_stripped]['__top_key'] = unsplit_value
                        parent_dict = parent_dict[new_key_stripped]

                    else:
                        # we are a kv pair if next item is peer
                        parent_dict[new_key_stripped] = unsplit_value

                elif nl_type == 'string':
                    parent_dict[new_key_stripped] = []
                    current_list = parent_dict[new_key_stripped]
                    if unsplit_value != '':
                        current_list.append(unsplit_value)

            elif current_type == 'tld':
                previous_indent = []
                parent_dict_stack = []
                new_key, unsplit_value = parse_line(line)
                if unsplit_value == '':
                    if nl_type == 'tld':
                        parsed_dict[new_key] = ''
                        parent_dict = parsed_dict
                    elif nl_type == 'string':
                        parsed_dict[new_key] = []
                        current_list = parsed_dict[new_key]
                    elif nl_type == 'subdict':
                        print('nl_subdict')
                        parsed_dict[new_key] = {}
                        parent_dict = parsed_dict[new_key]

                else:
                    # top-level dictionaries don't appear to support
                    # k: v + subdict
                    if nl_type == 'string':
                        parsed_dict[new_key] = [unsplit_value]
                        current_list = parsed_dict[new_key]
                    else:
                        parsed_dict[new_key] = unsplit_value

        items_out.append(parsed_dict)
    return items_out


def main():
    data = get_data()
    items = parse_data(data)
    print(json.dumps(items, indent=4))

if __name__ == '__main__':
    main()
